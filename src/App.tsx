import React from 'react';
import './ui/style.css'
import 'react-toastify/dist/ReactToastify.css';
import './services/firebase'
import { Provider, useSelector } from "react-redux";
import { store, persistor, GlobalStore } from './model/store';
import { Router } from "./router/Router";
import { Toast } from './features/toast';
import { PersistGate } from 'redux-persist/integration/react';
import PubNub from 'pubnub';
import { PubNubProvider } from 'pubnub-react';
import moment from 'moment';
moment.locale('ru')

export const pubnub = new PubNub({
  publishKey: 'pub-c-5653fccd-6868-4d9d-a20f-5081cef3232a',
  subscribeKey: 'sub-c-7b67b3bc-4321-11ec-96b3-4a48f5067549',
  secretKey: 'sec-c-NjBhNjg3MGItOWMxYy00OTFiLWFjNjUtM2I5MjFkNjA5NDFm',
  // uuid: 'DS'
});

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PubNubProvider client={pubnub}>
          <Toast />
          <Router />
        </PubNubProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
