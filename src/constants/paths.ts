export const PATHS = {
    root: '/',
    register: '/auth/registration ',
    auth: '/auth/auth',
    forgotpass: '/auth/forgotpass',
    updatepass: '/auth/updatepass',
    mainWildCard: '/main*',
    main: '/main',
    activeChats: '/main/active',
    chat: '/main/chat',
    completedChats: '/main/completed',
    savedChats: '/main/saved',
};
