import React from "react"
import styled from "styled-components"

import {useFormik} from "formik";
import { Button } from "../../ui"
import { Input } from "../../ui"
import {useDispatch, useSelector} from "react-redux";
import {GlobalStore} from "../../model/store";
import {Link} from "react-router-dom";
import {PATHS} from "../../constants";
import {resetpass} from "../../model/actions/auth";
import {toast} from "react-toastify";
import {validateForgotPassword} from "./Validators/ForgotPassword";

export const ForgotPasswordForm = () => {
    const authMessage = useSelector((store: GlobalStore) => store.authReducer.authMsg)
    const dispatch = useDispatch()
    React.useEffect(() =>{
        if(authMessage){
            toast(authMessage)
        }
    }, [authMessage])

    const formik = useFormik({
        initialValues:{
            email: ''
        },
        validate: validateForgotPassword,
        onSubmit: values => {
            dispatch(resetpass(values.email))
        }
    })

    return (
        <Wrapper>
            <FormWrapper onSubmit={formik.handleSubmit}>
                <h4>Восстановить пароль</h4>
                <div>
                    <p>Email</p>
                    <Input
                        placeholder={'Введите email'}
                        type='email'
                        name="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.email ? <div>{formik.errors.email}</div> : null}
                </div>
                <Button onClick={formik.handleSubmit}>
                    Отправить ссылку для восстановления
                </Button>
                <LinkWrapper>
                    <Link to={PATHS.auth}>Войти</Link>
                    <Link to={PATHS.register}>Регистрация</Link>
                </LinkWrapper>
            </FormWrapper>
        </Wrapper>
    );
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
`
const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 800px;
  min-width: 500px;
  border: 1px solid #ccc;
  border-radius: 8px;
  box-shadow: 0px 9px 27px 2px #11111133;
  padding: 16px;  
  font-family: 'Varela-round', 'Nunito';
`
const LinkWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 70%;
    margin-top: 8px
`