import React from "react"
import styled from "styled-components"
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {Link, useHistory} from "react-router-dom";
import GoogleButton from "react-google-button";
import {toast} from "react-toastify";

import { Button } from "../../ui"
import { Input } from "../../ui"

import {signin, signinGoogle} from "../../model/actions/auth";
import {GlobalStore} from "../../model/store";
import {PATHS} from "../../constants";
import {validateLogin} from "./Validators/Login";

export const LoginForm = () => {
    const authMessage = useSelector((store: GlobalStore) => store.authReducer.authMsg);
    const authorizedUser = useSelector<GlobalStore>((state) => state.authReducer.currentUser);
    const history = useHistory()
    const dispatch = useDispatch()

    React.useEffect(() => {
        if (authorizedUser) {
            history.push(PATHS.main)
        }
    },[authorizedUser])

    React.useEffect(() =>{
        if(authMessage){
            toast(authMessage)
        }
    }, [authMessage])

    const handleGoogleAuth = () => {
        dispatch(signinGoogle())
    }
    const formik = useFormik({
        initialValues:{
            email: '',
            password: ''
        },
        validate: validateLogin,
        onSubmit: values => {
            dispatch(signin(values.email, values.password))
        }
    })

    return (
        <Wrapper>
            <FormWrapper onSubmit={formik.handleSubmit}>
                <h4>Login</h4>
                <div>
                    <p>Email</p>
                    <Input
                        placeholder={'Введите email'}
                        type='email'
                        name="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.email ? <div>{formik.errors.email}</div> : null}
                </div>
                <div>
                    <p>Пароль</p>
                    <Input
                        name="password"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.password}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.password && formik.touched.password ? <div>{formik.errors.password}</div> : null}
                </div>
                <Button onClick={formik.handleSubmit} >
                    Войти
                </Button>
                <GoogleButton type='light' onClick={() => handleGoogleAuth()}/>
                <LinkWrapper>
                    <Link to={PATHS.register}>Зарегистрировать</Link>
                    <Link to={PATHS.forgotpass}>Забыли пароль?</Link>
                </LinkWrapper>
            </FormWrapper>
        </Wrapper>
    )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
`
const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 800px;
  min-width: 500px;
  border: 1px solid #ccc;
  border-radius: 8px;
  box-shadow: 0px 9px 27px 2px #11111133;
  padding: 16px;  
  font-family: 'Varela-round', 'Nunito';
`

const LinkWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 70%;
    margin-top: 8px
`