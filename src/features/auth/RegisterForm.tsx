import React from "react"
import styled from "styled-components"

import { useFormik } from "formik";
import { Button } from "../../ui"
import { Input } from "../../ui"
import { useDispatch, useSelector } from "react-redux";
import { GlobalStore } from "../../model/store";
import { signup } from "../../model/actions/auth";
import { PATHS } from "../../constants";
import { Link, useHistory } from "react-router-dom";
import {toast} from "react-toastify";
import {validateRegister} from "./Validators/Register";

export const RegisterForm = () => {
    const authMessage = useSelector((store: GlobalStore) => store.authReducer.authMsg);
    const authorizedUser = useSelector<GlobalStore>((state) => state.authReducer.currentUser);
    const history = useHistory()
    const dispatch = useDispatch()

    React.useEffect(() => {
        if (authorizedUser) {
            history.push(PATHS.main)
        }
    }, [authorizedUser])

    React.useEffect(() =>{
        if(authMessage){
            toast(authMessage)
        }
    }, [authMessage])

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            repeatPass: ''
        },
        validate: validateRegister,
        onSubmit: values => {
            dispatch(signup(values.email, values.password))
        }
    })

    return (
        <Wrapper>
            <FormWrapper onSubmit={formik.handleSubmit}>
                <h4>Registration</h4>
                <div>
                    <p>Email</p>
                    <Input
                        placeholder={'Введите email'}
                        type='email'
                        name="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                    />
                </div>
                {formik.errors.email ? <ErrWrap>{formik.errors.email}</ErrWrap> : null}
                <div>
                    <p>Пароль</p>
                    <Input
                        name="password"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.password}
                        onChange={formik.handleChange}
                    />
                </div>
                {formik.errors.password && formik.touched.password ? <ErrWrap>{formik.errors.password}</ErrWrap> : null}
                <div>
                    <p>Подтверждение пароля</p>
                    <Input
                        name="repeatPass"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.repeatPass}
                        onChange={formik.handleChange}
                    />
                </div>
                {formik.errors.repeatPass && formik.touched.repeatPass ? <ErrWrap>{formik.errors.repeatPass}</ErrWrap> : null}
                <Button onClick={formik.handleSubmit} >
                    Регистрация
                </Button>
                <LinkWrapper>
                    <Link to={PATHS.auth}>Войти</Link>
                    <Link to={PATHS.forgotpass}>Забыли пароль?</Link>
                </LinkWrapper>
            </FormWrapper>
        </Wrapper>
    )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
`
const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 800px;
  min-width: 500px;
  border: 1px solid #ccc;
  border-radius: 8px;
  box-shadow: 0px 9px 27px 2px #11111133;
  padding: 16px;  
  font-family: 'Varela-round', 'Nunito';
`

const ErrWrap = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    width: 400px;
    word-wrap: break-word;
    color: #007bff
`

const LinkWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 70%;
    margin-top: 8px
`