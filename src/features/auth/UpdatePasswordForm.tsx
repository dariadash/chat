import React from "react"
import styled from "styled-components"

import { useFormik } from "formik";
import { Button } from "../../ui"
import { Input } from "../../ui"
import { useDispatch, useSelector } from "react-redux";
import { GlobalStore } from "../../model/store";
import { Link, useHistory } from "react-router-dom";
import { PATHS } from "../../constants";
import { updatepass } from "../../model/actions/auth";
import {toast} from "react-toastify";
import {useQuery} from "../../lib";
import { validateUpdatePassword } from "./Validators/UpdatePassword";


export const UpdatePasswordForm = () => {
    const authMessage = useSelector((store: GlobalStore) => store.authReducer.authMsg)
    const query = useQuery()
    const dispatch = useDispatch()
    const history = useHistory()

    const [oobKey, setOobKey] = React.useState<string | null>(null)

    // Защита, если ключа не было
    React.useEffect(()=>{
        const oobKey = query.get('oobCode')
        if (!oobKey) {
            history.replace(PATHS.auth)
            return
        }
        setOobKey(oobKey)
    },[query])

    React.useEffect(() =>{
        if(authMessage){
            toast(authMessage)
        }
    }, [authMessage])

    const formik = useFormik({
        initialValues: {
            password: '',
            repeatPass: '',
        },
        validate: validateUpdatePassword,
        onSubmit: values => {
            if (oobKey) {
                dispatch(updatepass(values.password, oobKey))
                history.replace(PATHS.auth)
            }
        }
    })

    return (
        <Wrapper>
            <FormWrapper onSubmit={formik.handleSubmit}>
                <h4>Изменить пароль</h4>
                <div>
                    <p>Пароль</p>
                    <Input
                        name="password"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.password}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.password && formik.touched.password ? <div>{formik.errors.password}</div> : null}
                </div>
                <div>
                    <p>Подтверждение пароля</p>
                    <Input
                        name="repeatPass"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.repeatPass}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.password && formik.touched.password ? <div>{formik.errors.password}</div> : null}
                </div>
                <Button onClick={formik.handleSubmit} >
                    Отправить ссылку для восстановления
                </Button>
                <LinkWrapper>
                    <Link to={PATHS.auth}>Войти</Link>
                    <Link to={PATHS.register}>Регистрация</Link>
                </LinkWrapper>
            </FormWrapper>
        </Wrapper>
    )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
`
const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 800px;
  min-width: 500px;
  border: 1px solid #ccc;
  border-radius: 8px;
  box-shadow: 0px 9px 27px 2px #11111133;
  padding: 16px;  
  font-family: 'Varela-round', 'Nunito';
`
const LinkWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 70%;
    margin-top: 8px
`