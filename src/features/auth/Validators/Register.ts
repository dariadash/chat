type FormParams = {
    email?: string,
    password?: string,
    repeatPass?: string
}

export const validateRegister = (values: FormParams) => {
    const errors: FormParams = {};
    if (!values.password) {
        errors.password = 'Required';
    } else if (values.password.length > 8) {
        errors.password = 'Must be 8 characters or less';
    } else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,255}$/i.test(values.password)) {
        errors.password = 'Password must contain at least 8 characters, at least one number and both lower and uppercase letters';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }

    if (!values.repeatPass) {
        errors.repeatPass = 'Required';
    } else if (values.repeatPass !== values.password) {
        errors.repeatPass = 'The passwords are different';
    }

    return errors;
};