import React from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { PATHS } from '../../constants'
import { ChatData } from '../../interfaces'
import { saveChats } from '../../model/actions/chats'
import { selectChat } from '../../model/actions/messages'
import { GlobalStore } from '../../model/store'
import { ChatItem } from './ChatItem'

const CHAT_BY_PAGE = 1

export const ActiveDialogs = () => {
    const history = useHistory()
    const dispatch = useDispatch()
    const activeChats = useSelector((store: GlobalStore) => store.chatsReducer.activeChatsFiltered)
    const saveChatHandler = (id: string, index: number) => {
        dispatch(saveChats(id, 'active', index))
    }
    const takeChatHandler = (id: string, index: number) => {
        dispatch(saveChats(id, 'active', index))
    }

    const openChat = (chatData: ChatData) => {
        dispatch(selectChat(chatData))
        history.push(PATHS.chat)
    }

    const [chats, setChats] = React.useState<ChatData[]>([])
    const [page, setPage] = React.useState<number>(0)
    const hasMore = chats.length < activeChats.length
    const loadFunc = () => {
        console.log('loadFunc')
        const newChats = activeChats.slice(page * CHAT_BY_PAGE, page * CHAT_BY_PAGE + CHAT_BY_PAGE)
        setChats([...chats, ...newChats])
        setPage(page + 1)
    }

    return (<div>
        <InfiniteScroll
            pageStart={0}
            loadMore={loadFunc}
            hasMore={hasMore}
            loader={<div className="loader" key={0}>Loading ...</div>}
            useWindow={false}
        >
            {chats.map(
                (chatData, index) => (
                    <ChatItem
                        key={index}
                        lastMessage={chatData.messages[chatData.messages.length - 1]}
                        operatorId={chatData.operatorId}
                        status={chatData.status}
                        taken={chatData.taken}
                        saved={chatData.saved}
                        onSave={() => saveChatHandler(chatData.roomDocumentId, index)}
                        onTake={() => takeChatHandler(chatData.roomDocumentId, index)}
                        onOpen={() => openChat(chatData)}
                    />
                )
            )}
        </InfiniteScroll>
    </div>)
}
