import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { Hint } from 'react-autocomplete-hint';
import { usePubNub } from 'pubnub-react';
import { TypingIndicator } from "@pubnub/react-chat-components";
import Picker from 'emoji-picker-react';
import styled from 'styled-components'
import { GlobalStore } from '../../model/store'
import { Button, Icon, Popper } from '../../ui'
import { sendMessage } from '../../model/actions/messages'
import { toast } from 'react-toastify'
import { dateToFromNowDaily } from '../../lib/date'
import { PhrasesDropdown } from './PhrasesDropdown'
import { PhrasesSettings } from '../settings/PhrasesSettings'
import { Modal } from '../../ui/Modal'
import { Typing } from '../typing/Typing';
import { channel } from '@redux-saga/core';

export const Chat = () => {
    const pubnub = usePubNub();
    const history = useHistory()
    const dispatch = useDispatch()
    const currentRoomData = useSelector((store: GlobalStore) => {
        return store.messagesReducer.currentRoom
    })
    const currentPhrases = useSelector((store: GlobalStore) => {
        return store.phrasesReducer.phrases
    })
    const [showModal, setShowModal] = React.useState(false)
    const [messageText, setMessageText] = React.useState<string>('')

    const handleSendText = React.useCallback((message: any) => {
        const messageTrimmed = messageText.trim()
        if (messageTrimmed.length === 0) {
            toast('Нельзя отправить пустое сообщение')
            return
        }
        if (messageText && currentRoomData) {
            pubnub
                .publish({ channel: currentRoomData.roomDocumentId, message })
                .then(() => setMessageText(''));
        }
        dispatch(sendMessage(messageText))
        console.log(currentRoomData?.roomDocumentId)
    }, [currentRoomData, messageText])



    if (!currentRoomData) {
        history.replace('/main')
        return null
    }

    const onEmojiClick = (event: any, emojiObject: any) => {
        setMessageText(messageText + emojiObject.emoji)
    };

    return (
        <>
            <Typing channels={currentRoomData.roomDocumentId} />
            {showModal && (
                <Modal
                    visible={showModal}
                    onClose={() =>
                        setShowModal(false)}
                >
                    <PhrasesSettings />
                </Modal>
            )}
            <Container>
                {currentRoomData.messages.map(
                    (message, index) => (
                        <Cloud key={index}>
                            <div>
                                <div>
                                    {message.writtenBy}: &nbsp;
                                    {message.content} &nbsp;
                                </div>
                                <div>
                                    {message.picture && (<Image src={message.picture} />)}
                                </div>
                            </div>
                            <div>
                                {dateToFromNowDaily(new Date(message.timestamp.seconds
                                    * 1000 + message.timestamp.nanoseconds /
                                    1000000).toDateString())} &nbsp;
                                {new Date(message.timestamp.seconds * 1000 +
                                    message.timestamp.nanoseconds /
                                    1000000).toLocaleTimeString("ru-RU",
                                        { hour: 'numeric', minute: 'numeric' })
                                }
                            </div>
                        </Cloud>
                    )
                )}
            </Container>
            <InfoWrapper>
                {/* <TypingIndicator /> */}
                {currentRoomData.status === 'completed' && `Диалог завершился ${dateToFromNowDaily(new Date(
                    currentRoomData.messages.slice(-1)[0].timestamp.seconds *
                    1000 + currentRoomData.messages.slice(-1)[0].timestamp.nanoseconds /
                    1000000).toDateString())}`}
            </InfoWrapper>
            <InputWrapper>
                <InputMessage>
                    <p>Введите ответ:</p>
                    <MessageWrapper>
                        <Hint options={currentPhrases}>
                            <input
                                style={{
                                    minWidth: '350px',
                                    minHeight: '50px',
                                    flexDirection: 'row',
                                    fontSize: '16px',
                                    padding: '10px',
                                    borderRadius: '8px',
                                    background: '#FFFFFF',
                                    border: '1px solid #CCC',
                                    color: '#080111'
                                }}
                                value={messageText}
                                onKeyPress={e => {
                                    if (e.key !== 'Enter') return;
                                    handleSendText(messageText);
                                }}
                                onChange={e => setMessageText(e.target.value)} />
                        </Hint>
                        <div>
                            <Popper>
                                <Picker
                                    native={true}
                                    onEmojiClick={onEmojiClick} />
                            </Popper>
                        </div>
                    </MessageWrapper>
                    <Button
                        onClick={e => {
                            e.preventDefault();
                            handleSendText(messageText);
                        }}
                    >
                        Отправить
                    </Button>
                </InputMessage>
                <InputMessage>
                    <SettingsWrapper onClick={() => {
                        setShowModal(true)
                    }}>
                        <Icon icon='settings' /> &nbsp;
                        Настройки
                    </SettingsWrapper>
                    <PhrasesDropdown />
                </InputMessage>
            </InputWrapper>
        </>
    )
}

const Container = styled.div`
    box-sizing: border-box;
    height: 100%;
    width: 100%;
    display:flex;
    flex-direction: column;
    overflow-x: hidden;
`

const Cloud = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 98%;
    background-color: #ffffff;
    border: 1px solid #ccc;
    border-radius: 6px;
    margin: 4px;
    padding: 4px;
`

const Image = styled.img`
    width: 27%;
    background: #f0f0f0;
    border-radius: 5px;
    padding: 4px;
`

const InfoWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`

const InputWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    height: 50%;
    width: 98%;
    background-color: #ffffff;
    border: 1px solid #ccc;
    border-radius: 6px;
`

const InputMessage = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
    border: 1px solid #ccc;
    border-radius: 6px;
    padding: 4px;
`

const MessageWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    box-sizing: border-box;
    position: relative;
    width: 100%;
    padding: 6px;
`

const SettingsWrapper = styled.div`
    cursor: pointer;
`
