import moment from 'moment'
import 'moment/locale/ru'
import styled from 'styled-components'
import { Icon } from '../../ui'
moment.locale('ru')

type Props = {
    lastMessage: {
        writtenBy: string,
        content: string,
        timestamp: {
            nanoseconds: number,
            seconds: number
        }
    },
    operatorId: string,
    status: string,
    taken: Boolean,
    saved: Boolean,
    onSave?: () => void,
    onTake?: () => void,
    onOpen?: () => void
}

export const ChatItem = ({ lastMessage, operatorId, status, taken, saved, onSave, onTake, onOpen }: Props) => {

    const fireBaseTime = new Date(
        lastMessage.timestamp.seconds * 1000 + lastMessage.timestamp.nanoseconds / 1000000,
    );
    // const date = fireBaseTime.toDateString();
    // const atTime = fireBaseTime.toLocaleTimeString();
    const xDaysAgo = moment(fireBaseTime).fromNow()

    return (
        <ItemWrapper>
            <div>{operatorId}</div>
            <TextWrapper>{lastMessage && onOpen && (
                <div onClick={onOpen}>
                    <div>{lastMessage.writtenBy}</div>
                    <div>{lastMessage.content}</div>
                </div>
            )}
            </TextWrapper>
            <TextWrapper>
                {status === 'active' && (
                    <>
                        {!taken && (
                            <SelectWrapper onClick={onTake}>
                                <p>Новый!</p>
                                <p>Войти в диалог</p>
                            </SelectWrapper>
                        )}
                        {taken && (
                            <div>
                                <p>Продолжить</p>
                            </div>
                        )}
                    </>
                )}
                <div>{xDaysAgo}</div>
                {status === 'completed' && <div>Оценка 0/5</div>}
                {taken
                    ? <>
                        {!saved
                            ? <SelectWrapper onClick={onSave}>Сохранить <Icon icon='save' /></SelectWrapper>
                            : <SelectWrapper>Удалить <Icon icon='delete' /></SelectWrapper>
                        }
                    </>
                    : <></>
                }
            </TextWrapper>
        </ItemWrapper>
    )
}

const ItemWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    align-content: stretch;
    width: 100%;
    min-height: 150px;
    box-sizing: border-box;
    color: #333;
    cursor: pointer;
    &:hover {
        background: #d7d7d7;
        transition: 0.3s;
    }
    border-radius: 6px;
    padding: 10px;
`

const TextWrapper = styled.div`
    justify-content: space-between;
    padding: 8px;
`

const SelectWrapper = styled.div`
    &:hover {
        text-decoration: underline
    }
`