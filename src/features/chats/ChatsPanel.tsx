import { useHistory } from "react-router";
import styled from "styled-components";
import { PATHS } from "../../constants";
import { Button, Icon } from "../../ui";

export const ChatsPanel = () => {
  const history = useHistory()
  return (
    <DialogsListWrapper>
      <HeaderWrapper>HELPERS</HeaderWrapper>
      <Button onClick={() => history.push(PATHS.activeChats)}>
        <InButtonWrapper>
          <Icon icon='important' size='4x' />
          <p>Активные</p>
        </InButtonWrapper>
      </Button>
      <Button onClick={() => history.push(PATHS.completedChats)}>
        <InButtonWrapper>
          <Icon icon='done' size='4x' />
          <p>Завершенные</p>
        </InButtonWrapper>
      </Button>
      <Button onClick={() => history.push(PATHS.savedChats)}>
        <InButtonWrapper>
          <Icon icon='favs' size='4x' />
          <p>Сохраненные</p>
        </InButtonWrapper>
      </Button>
    </DialogsListWrapper>
  )
}

const DialogsListWrapper = styled.div`
  display: flex;
  flex: 0;
  flex-direction: column;
  justify-content: start;
  align-items: center;
  width: 100%;
  background-color: #007bff;
  border-radius: 0px 8px 8px 0px;
  padding: 8px;
  color: #ffffff;
`

const InButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const HeaderWrapper = styled.h1`
  font-size: 32px;
  margin: 8px;
`