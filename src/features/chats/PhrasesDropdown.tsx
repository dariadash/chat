import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { sendMessage } from '../../model/actions/messages'
import { GlobalStore } from '../../model/store'
import { Dropdown } from '../../ui'


export const PhrasesDropdown = () => {
    const dispatch = useDispatch()
    const currentPhrases = useSelector((store: GlobalStore) => {
        return store.phrasesReducer.phrases
    })
    return (
        <Dropdown
            options={currentPhrases.map(
                (phrase) => ({
                    value: phrase,
                    text: phrase
                })
            )}
            placeholder={'Выберите фразу для отправки'}
            onOptionChange={(str) => dispatch(sendMessage(str as string))}
        />
    )
}