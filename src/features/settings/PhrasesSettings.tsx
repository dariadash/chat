import React from 'react'
import { ErrorMessage, Field, FieldArray, Form, Formik } from 'formik';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalStore } from '../../model/store';
import { setPhrases } from '../../model/actions/settings';
import { Button } from '../../ui';

export const PhrasesSettings = () => {
    const dispatch = useDispatch()
    const currentPhrases = useSelector((store: GlobalStore) => {
        return store.phrasesReducer
    })
    return (
        <Wrapper>
            <h2>Настройки диалогов</h2>
            <Formik
                initialValues={currentPhrases}
                onSubmit={async ({ phrases }) => {
                    dispatch(setPhrases(phrases))
                }}
            >
                {({ values }) => (
                    <Form>
                        <FormWrapper>
                            <p>Готовые фразы:</p>
                        </FormWrapper>
                        <FieldArray name="phrases">
                            {({ insert, remove, push }) => (
                                <FormWrapper>
                                    {values.phrases.length > 0 &&
                                        values.phrases.map((phrases, index) => (
                                            <FormItemWrapper className="row" key={index}>
                                                <Field
                                                    name={`phrases.${index}`}
                                                    placeholder="введите фразу"
                                                    type="text"
                                                />
                                                <ErrorMessage
                                                    name={`phrases.${index}`}
                                                    component="div"
                                                    className="field-error"
                                                />
                                                <button
                                                    type="button"
                                                    className="secondary"
                                                    onClick={() => remove(index)}
                                                >
                                                    X
                                                </button>
                                            </FormItemWrapper>
                                        ))}
                                    <FormItemWrapper>
                                        <button
                                            type="button"
                                            className="secondary"
                                            onClick={() => push('')}
                                        >
                                            Добавить фразу
                                        </button>
                                    </FormItemWrapper>
                                    <FormItemWrapper>
                                        <p>Автоматическое приветствие: &nbsp; </p>
                                        <Field
                                            name={`phrases`}
                                            placeholder="введите фразу"
                                            type="text"
                                        />
                                    </FormItemWrapper>
                                </FormWrapper>
                            )}
                        </FieldArray>
                        <Button type="submit">Сохранить</Button>
                    </Form>
                )}
            </Formik>
        </Wrapper>
    )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  min-width: 300px;
  min-height: 300px;
`

const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  /* margin: 8px; */
`
const FormItemWrapper = styled.form`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  min-width: 300px;
  padding: 6px;
`