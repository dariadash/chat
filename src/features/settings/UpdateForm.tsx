import React from "react"
import { useFormik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import styled from "styled-components"
import { updatepass } from "../../model/actions/auth"
import { GlobalStore } from "../../model/store"
import { Button, Input } from "../../ui"
import { validateUpdatePassword } from "../auth/Validators/UpdatePassword"


export const UpdateForm = () => {
    const authMessage = useSelector((store: GlobalStore) => store.authReducer.authMsg)
    const dispatch = useDispatch()
    // const authorisedUser = 
    // const handleUpdate = (e?: React.FormEvent<HTMLFormElement>) => {
    //     e?.preventDefault()
    //     updateInfoFx({
    //         about
    //     })
    // }

    // React.useEffect(() => {
    //     setAbout(authorisedUser?.about || '')
    // },[])

    // const updateHandler = (files) => {
    //     updateAvatarFx(files)
    // }

    const formik = useFormik({
        initialValues: {
            password: '',
            repeatPass: '',
        },
        validate: validateUpdatePassword,
        onSubmit: values => {
            dispatch(updatepass(values.password))
        }
    })
    return (
        <Wrapper>
            <FormWrapper onSubmit={formik.handleSubmit}>
                <h4>Обновить профиль</h4>
                <div>
                    Имя:
                    {/* <Input
                        placeholder={''}
                        type=''
                        value={}
                        onChange={(text) => {}}
                    /> */}
                </div>
                <div>
                    <p>Пароль</p>
                    <Input
                        name="password"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.password}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.password && formik.touched.password ? <div>{formik.errors.password}</div> : null}
                </div>
                <div>
                    <p>Подтверждение пароля</p>
                    <Input
                        name="repeatPass"
                        placeholder={'Введите пароль'}
                        type='password'
                        value={formik.values.repeatPass}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.password && formik.touched.password ? <div>{formik.errors.password}</div> : null}
                </div>
                <Button onClick={formik.handleSubmit} >
                    Обновить профиль
                </Button>
            </FormWrapper>
        </Wrapper>
    )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 300px;
  min-height: 300px;
`
const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  min-width: 300px;
  padding: 16px;
`