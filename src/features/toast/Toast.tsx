import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast, ToastContainer } from 'react-toastify'
import { GlobalStore } from '../../model/store'
import { Toasts } from '../../sagas/actionTypes'


export const Toast = () => {
    const toastData = useSelector((state: GlobalStore) => state.toastReducer.toastData)
    const dispatch = useDispatch()

    React.useEffect(() => {
        if (toastData) {
            if (toastData.type === 'success') {
                toast.success(toastData.text)
            }
            if (toastData.type === 'error') {
                toast.error(toastData.text)
            }
            dispatch({ type: Toasts.CLEAR })
        }
    }, [toastData])

    return (
        <ToastContainer />
    )
}