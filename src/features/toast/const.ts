export enum ToastMessages {
    signupSuccess = 'Ваш аккаунт успешно создан!',
    signupError = 'Аккаунт не создан',
    signinError = 'Ошибка входа',
    signoutSuccess = 'Успешно вышли',
    signoutError = 'Ошибка выхода',
    resetEmailSuccess = 'Сообщение с ссылкой для сброса отправлено',
    resetEmailError = 'Ошибка отправки сообщения для сброса пароля',
    updatePassSuccess = 'Пароль успешно обновлен',
    updatePassError = 'Ошибка обновления пароля',
    updateSettingsSuccess = 'Профиль обновлен',
    updateSettingsError = 'Ошибка обновления профиля'
}