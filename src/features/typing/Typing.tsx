import React from 'react'
import { MessageEvent } from 'pubnub'
import { usePubNub } from 'pubnub-react';

type Props = {
    channels: string,
}

export const Typing = ({ channels }: Props) => {
    const pubnub = usePubNub();
    const handleMessage = (event: MessageEvent) => {
        console.log('ХАНДЛИМ СООБЩЕНИЕ')
        console.log(event)
        const message = event.message;
        if (typeof message === 'string' || message.hasOwnProperty('text')) {
            const text = message.text || message;
            // addMessage(messages => [...messages, text]);
            console.log('Пришло сообщение')
        }
    };

    React.useEffect(() => {

        console.log('Typing useeffect')
        pubnub.addListener({ message: handleMessage });
        pubnub.subscribe({ channels: [channels] });
        // pubnub.signal({
        //     message: messageText,
        //     channel: channels[0],
        // }, (status, response) => {
        //     console.log(status, response)
        // })

        // pubnub.fetchMessages(
        //     {
        //         channels: channels,
        //         end: '15343325004275466',
        //         count: 10
        //     },
        //     (status, response) => {
        //         console.log(status, response);
        //     }
        // );

        return () => {
            pubnub.removeListener({ message: handleMessage })
            pubnub.unsubscribeAll()
        }
    }, [pubnub, channels]);

    //(function() {
    //     const pubnub = new PubNub({ publishKey: 'YOUR_PUBNUB_PUBLISH_KEY', subscribeKey: 'YOUR_PUBNUB_SUBSCRIBE_KEY' }); // Your PubNub keys here. Get them from https://dashboard.pubnub.com.
    //     let box = document.getElementById("box"),
    //         input = document.getElementById("input"),
    //         typingIndicator = document.getElementById("typing-indicator"),
    //         typingUserId = document.getElementById("typing-user-id");
    //         chatMessageChannel = 'chat-signals-example',
    //         timeoutCache = 0,
    //         isTyping = false,
    //         isTypingChannel = 'is-typing';
    //     let hideTypingIndicator = () => {
    //         isTyping = false;
    //         typingIndicator.style = "visibility:hidden;";
    //     };
    //     pubnub.subscribe({ channels: [chatMessageChannel, isTypingChannel] }); // Subscribe to a channel.
    //     pubnub.addListener({
    //         message: function(m) {
    //             clearTimeout(timeoutCache);
    //             hideTypingIndicator(); // When a message has been sent, hide the typing indicator
    //             box.innerHTML = ('' + m.message).replace(/[<>]/g, '') + '<br>' + box.innerHTML; // Add message to page.
    //         },
    //         signal: function(s) {
    //             clearTimeout(timeoutCache);
    //             typingIndicator.style = "";
    //             typingUserId.innerText = s.publisher; // the UUID of the user who is typing
    //             timeoutCache = setTimeout(hideTypingIndicator, 10000) // 10 seconds
    //             if (s.message === '0') {
    //                 hideTypingIndicator();
    //             }
    //         }
    //     });
    //     input.addEventListener('keyup', function(e) {
    //         // Publish new PubNub message when return key is pressed.
    //         if ((e.keyCode || e.charCode) === 13 && input.value.length > 0) {
    //             pubnub.publish({
    //                 channel: chatMessageChannel,
    //                 message: input.value
    //             });
    //             input.value = '';
    //         }
    //         const inputHasText = input.value.length > 0;
    //         // Publish new PubNub signal: Typing Indicator ON (1) or OFF (2)
    //         if ((inputHasText && !isTyping) || (!inputHasText && isTyping)) {
    //             isTyping = !isTyping;
    //             pubnub.signal({
    //                 channel: isTypingChannel,
    //                 message: inputHasText ? '1' : '0'
    //             });
    //         }
    //     });
    // })();

    return <></>
}