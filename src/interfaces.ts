
export type ChatData = {
    operatorId: string,
    status: "active" | "completed",
    saved: Boolean,
    taken: Boolean,
    roomDocumentId: string,
    messages: {
        picture?: string,
        content: string,
        writtenBy: string,
        timestamp: {
            seconds: number,
            nanoseconds: number
        }
    }[]
}
