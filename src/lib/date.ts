import 'moment/locale/ru'
import moment from "moment";

export function dateToFromNowDaily(myDate: any) {
    // var fromNow = moment(myDate).fromNow();
    return moment(myDate).calendar(null, {
        lastWeek: '[В] dddd',
        lastDay: '[Вчера]',
        sameDay: '[Сегодня]',
        nextDay: '[Завтра]',
        // nextWeek: 'dddd',            
        // sameElse: function () {
        //     // return "[" + fromNow + "]";
        //     return "dddd"
        // }
    });
}