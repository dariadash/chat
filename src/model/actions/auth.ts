import { User } from "@firebase/auth"
import {
    SIGNIN_ERROR,
    SIGNIN_REQUEST,
    SIGNIN_SUCCESS,
    SIGNOUT_REQUEST,
    SIGNOUT_SUCCESS,
    SIGNUP_REQUEST,
    RESETPASS_REQUEST,
    UPDATEPASS_REQUEST,
    SIGNIN_GOOGLE_POPUP
} from "../../sagas/actionTypes"

export const signinSuccess = (user: User) => {
    return { type: SIGNIN_SUCCESS, payload: user }
}
export const signinError = (error?: any) => {
    return { type: SIGNIN_ERROR, payload: "Invalid auth credentials" || error }
}
export const signin = (email: string, password: string) => {
    return { type: SIGNIN_REQUEST, payload: { email, password } }
}
export const signinGoogle = () => {
    return { type: SIGNIN_GOOGLE_POPUP }
}

export const signoutSuccess = () => {
    return { type: SIGNOUT_SUCCESS }
}
export const signout = () => {
    return { type: SIGNOUT_REQUEST, payload: 'Вы вышли' }
}

export const signup = (email: string, password: string) => {
    return { type: SIGNUP_REQUEST, payload: { email, password } }
}

export const updatepass = (password: string, oobKey?: string) => {
    return { type: UPDATEPASS_REQUEST, payload: { password, oobKey } }
}

export const resetpass = (email: string) => {
    return { type: RESETPASS_REQUEST, payload: { email } }
}
