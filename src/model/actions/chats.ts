import { Chats } from "../../sagas/actionTypes";

export const fetchAllChats = () => ({ type: Chats.FETCH_ALL })
export const saveChats = (roomId: string, type: 'active' | 'completed', chatIndex: number) =>
({
    type: Chats.SAVE_OR_UNSAVE_CHAT, payload: {
        roomId, type, chatIndex
    }
})
export const takeChats = (roomId: string, type: 'active', chatIndex: number) =>
({
    type: Chats.TAKE_CHAT, payload: {
        roomId, type, chatIndex
    }
})