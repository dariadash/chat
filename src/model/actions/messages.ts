import { ChatData } from "../../interfaces";
import { Messages } from "../../sagas/actionTypes";

export const selectChat = (chat: ChatData) => {
    return { type: Messages.OPEN_CHAT, payload: chat }
}

export const sendMessage = (text: string) => {
    return {
        type: Messages.SEND_MESSAGE, payload: text
    }
}