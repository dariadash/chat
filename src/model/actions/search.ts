import { Chats } from "../../sagas/actionTypes";

export function search(filter: string) {
    return { type: Chats.FILTER_CHATS, payload: filter };
}