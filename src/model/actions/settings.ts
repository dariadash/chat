import { User } from "@firebase/auth"
import { Phrase, Settings } from "../../sagas/actionTypes"


export const takePhrases = () => {
    return { type: Phrase.SELECT_PHRASE }
}
export const setPhrases = (phrases: string[]) => {
    return { type: Phrase.SET_PHRASE, payload: phrases }
}


export const updateSettings = () => {
    return { type: Settings.UPDATE_SETTINGS }
}
export const updateSuccess = (user: User) => {
    return { type: Settings.UPDATE_SUCCESS, payload: user }
}
export const updateError = (error?: any) => {
    return { type: Settings.UPDATE_FAIL, payload: error }
}