import { Toasts } from "../../sagas/actionTypes"

export const showToast = (text: string, type: 'success' | 'error') => {
    return {
        type: Toasts.SHOW, payload: {
            text,
            type
        }
    }
}