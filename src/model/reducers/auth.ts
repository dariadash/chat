import { User } from "@firebase/auth";
import {
    SIGNIN_SUCCESS,
    SIGNIN_ERROR,
    SIGNOUT_SUCCESS,
} from "../../sagas/actionTypes";

export type AuthReducer = {
    authMsg?: string,
    currentUser: User | null,
    error: Boolean
}

const INITIAL_STATE: AuthReducer = {
    authMsg: "",
    currentUser: null,
    error: false,
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = INITIAL_STATE, action: any) {
    if (action.type === SIGNIN_SUCCESS) {
        return { ...state, authMsg: "", currentUser: action.payload, error: false };
    } else if (action.type === SIGNIN_ERROR) {
        return { ...state, authMsg: action.payload, error: true };
    } else if (action.type === SIGNOUT_SUCCESS) {
        return { ...state, currentUser: null, }
    } else {
        return state;
    }
}
