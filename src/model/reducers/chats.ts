import { ChatData } from "../../interfaces";
import { Chats } from "../../sagas/actionTypes";

const INITIAL_STATE: ChatsReducer = {
    activeChats: [],
    completedChats: [],
    savedChats: [],
    activeChatsFiltered: [],
    completedChatsFiltered: [],
    savedChatsFiltered: [],
}

export type ChatsReducer = {
    activeChats: ChatData[],
    completedChats: ChatData[],
    savedChats: ChatData[],
    activeChatsFiltered: ChatData[],
    completedChatsFiltered: ChatData[],
    savedChatsFiltered: ChatData[],
}

export const chatsReducer = (state = INITIAL_STATE, action: { payload: any, type: Chats }) => {
    switch (action.type) {
        case Chats.SET_CHATS: {
            const activeChats = action.payload.filter(
                (chat: any) => chat.status === 'active'
            );
            const completedChats = action.payload.filter(
                (chat: any) => chat.status === 'completed'
            )
            const savedChats = action.payload.filter(
                (chat: any) => chat.saved
            )
            return {
                ...state,
                activeChats,
                completedChats,
                savedChats,
                activeChatsFiltered: activeChats,
                completedChatsFiltered: completedChats,
                savedChatsFiltered: savedChats,
            };
        }
        case Chats.FILTER_CHATS: {
            // search reset
            if (action.payload === '') {
                return {
                    ...state,
                    activeChatsFiltered: state.activeChats,
                    completedChatsFiltered: state.completedChats,
                    savedChatsFiltered: state.savedChats,
                }
            }
            const activeChatsFiltered = state.activeChats.filter(
                (chat) => {
                    const hasMessageWithPayload = chat.messages.some(
                        (message) => message.content.includes(action.payload)
                    )
                    return hasMessageWithPayload || chat.operatorId.includes(action.payload)
                }
            )
            const completedChatsFiltered = state.completedChats.filter(
                (chat) => {
                    const hasMessageWithPayload = chat.messages.some(
                        (message) => message.content.includes(action.payload)
                    )
                    return hasMessageWithPayload || chat.operatorId.includes(action.payload)
                }
            )
            const savedChatsFiltered = state.savedChats.filter(
                (chat) => {
                    const hasMessageWithPayload = chat.messages.some(
                        (message) => message.content.includes(action.payload)
                    )
                    return hasMessageWithPayload || chat.operatorId.includes(action.payload)
                }
            )
            return {
                ...state,
                activeChatsFiltered,
                completedChatsFiltered,
                savedChatsFiltered
            };
        }
        case Chats.SET_SAVE_CHAT: {
            const { payload } = action
            let newState = {
                ...state,
                activeChats: state.activeChats.map((el) => ({ ...el })),
                completedChats: state.completedChats.map((el) => ({ ...el })),
                savedChats: state.savedChats.map((el) => ({ ...el })),
                activeChatsFiltered: state.activeChatsFiltered.map((el) => ({ ...el })),
                completedChatsFiltered: state.completedChatsFiltered.map((el) => ({ ...el })),
                savedChatsFiltered: state.savedChatsFiltered.map((el) => ({ ...el })),
            }
            console.log(payload)
            switch (payload.type) {
                case 'active': newState.activeChatsFiltered[payload.chatIndex].saved = !newState.activeChatsFiltered[payload.chatIndex].saved;
                    break;
                case 'completed': newState.completedChatsFiltered[payload.chatIndex].saved = !newState.completedChatsFiltered[payload.chatIndex].saved;
                    break;
            }
            return newState
        }

        case Chats.TAKE_CHAT: {
            const { payload } = action
            let newState = {
                ...state,
                activeChats: state.activeChats.map((el) => ({ ...el })),
                completedChats: state.completedChats.map((el) => ({ ...el })),
                savedChats: state.savedChats.map((el) => ({ ...el })),
                activeChatsFiltered: state.activeChatsFiltered.map((el) => ({ ...el })),
                completedChatsFiltered: state.completedChatsFiltered.map((el) => ({ ...el })),
                savedChatsFiltered: state.savedChatsFiltered.map((el) => ({ ...el })),
            }
            switch (payload.type) {
                case 'active': newState.activeChatsFiltered[payload.chatIndex].taken = !newState.activeChatsFiltered[payload.chatIndex].taken;
                    break;
                case 'completed': newState.completedChatsFiltered[payload.chatIndex].taken = !newState.completedChatsFiltered[payload.chatIndex].taken;
                    break;
            }

            return newState
        }
    }
    return { ...state }
}
