import { combineReducers } from "redux";
import { firebaseReducer } from "react-redux-firebase";
import authReducer from "./auth";
import { chatsReducer } from "./chats";
import { toastReducer } from "./toast";
import { phrasesReducer } from "./settings";
import { messagesReducer } from './messages'

export default combineReducers({
    firebaseReducer,
    authReducer,
    toastReducer,
    chatsReducer,
    phrasesReducer,
    messagesReducer
});
