import { ChatData } from "../../interfaces";
import { Messages } from "../../sagas/actionTypes";

export type MessagesReducer = {
    currentRoom: ChatData | null,
}

const INITIAL_STATE: MessagesReducer = {
    currentRoom: null
}

export const messagesReducer = (state = INITIAL_STATE, action: { payload: any, type: Messages }) => {
    if (action.type === Messages.OPEN_CHAT) {
        return { ...state, currentRoom: action.payload }
    }
    if (action.type === Messages.SEND_MESSAGE_LOCAL) {
        if (!state.currentRoom) {
            return { ...state }
        }
        const newMessages = [...state.currentRoom.messages, {
            picture: action.payload.picture,
            content: action.payload.content,
            writtenBy: action.payload.writtenBy,
            timestamp: {
                seconds: new Date().getSeconds(),
                nanoseconds: new Date().getMilliseconds() / 1000,
            }
        }]
        return {
            ...state, currentRoom: {
                ...state.currentRoom,
                messages: newMessages
            }
        }
    }
    if (action.type === Messages.CLOSE_CHAT) {
        return {
            ...state,
            currentRoom: null
        }
    }
    return { ...state }
}