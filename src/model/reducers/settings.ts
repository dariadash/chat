import { Phrase } from "../../sagas/actionTypes";

export type PhrasesReducer = {
    phrases: string[]
}

const INITIAL_STATE: PhrasesReducer = {
    phrases: []
}

export const phrasesReducer = (state = INITIAL_STATE, action: any) => {
    if (action.type === Phrase.SELECT_PHRASE) {
        const phrases = action.payload.filter(
            (frase: any) => frase === frase
        );
        return { ...state, phrases };
    }
    if (action.type === Phrase.SET_PHRASE) {
        const phrasesCopy: Phrase[] = action.payload.map((el: Phrase) => el) as Phrase[]
        return { ...state, phrases: phrasesCopy };
    }

    return state;
}
