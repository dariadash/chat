import {
    Toasts
} from "../../sagas/actionTypes";


export type ToastReducer = {
    toastData?: {
        text: string,
        type: 'success' | 'error'
    }
}

const INITIAL_STATE: ToastReducer = {
};

// eslint-disable-next-line import/no-anonymous-default-export
export const toastReducer = (state = INITIAL_STATE, action: { payload: any, type: Toasts }) => {
    if (action.type === Toasts.SHOW) {
        return { toastData: action.payload }
    }
    if (action.type === Toasts.CLEAR) {
        return {}
    }
    return { ...state }
}
