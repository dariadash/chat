import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from 'redux-saga'
import mainReducer from "./reducers";
import { AuthReducer } from "./reducers/auth";
import { ToastReducer } from "./reducers/toast";
import rootSaga from "../sagas";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { ChatsReducer } from "./reducers/chats";
import { MessagesReducer } from "./reducers/messages";
import { PhrasesReducer } from './reducers/settings'

export type GlobalStore = {
    authReducer: AuthReducer,
    toastReducer: ToastReducer,
    chatsReducer: ChatsReducer,
    messagesReducer: MessagesReducer,
    phrasesReducer: PhrasesReducer
}

const persistConfig = {
    key: 'root',
    storage,
}


const composeSetup = process.env.NODE_ENV !== 'production' && typeof window === 'object' &&
    // @ts-ignore
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    // @ts-ignore
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose

const sagaMiddleware = createSagaMiddleware()
const persistedReducer = persistReducer(persistConfig, mainReducer)
export const store = createStore(
    persistedReducer,
    composeSetup(applyMiddleware(sagaMiddleware))
);
export const persistor = persistStore(store)
sagaMiddleware.run(rootSaga)