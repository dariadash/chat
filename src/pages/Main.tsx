import React from 'react'
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from "react-router";
import styled from "styled-components";
import { PATHS } from "../constants";
import { ChatsPanel, ActiveDialogs, SavedDialogs } from "../features/chats";
import { Chat } from "../features/chats/Chat";
import { CompletedDialogs } from "../features/chats/CompletedDialogs";
import { signout } from "../model/actions/auth";
import { search } from "../model/actions/search";
import { GlobalStore } from "../model/store";
import { Button, Input } from "../ui";
import debounce from 'lodash.debounce'
import { fetchAllChats } from '../model/actions/chats';
import { Modal } from '../ui/Modal';
import { UpdateForm } from '../features/settings/UpdateForm';

export const Main = () => {
  const authorizedUser = useSelector((state: GlobalStore) => state.authReducer.currentUser);
  const dispatch = useDispatch()
  const logout = () => {
    dispatch(signout())
  }
  React.useEffect(() => {
    dispatch(fetchAllChats())
  }, [])
  const [showModal, setShowModal] = React.useState(false)
  const [searchText, setSearchText] = React.useState('')
  const searchActionDebounced = debounce((text: string) => {
    dispatch(search(text))
  }, 1000)
  const handleSearchText = (text: string) => {
    setSearchText(text)
    searchActionDebounced(text)
  }

  const pendingClients = useSelector(
    (store: GlobalStore) => store.chatsReducer.activeChats.filter(
      (chat) => !chat.taken
    ).length
  )

  if (!authorizedUser) {
    return null
  }

  return (
    <Wrapper>
      {showModal && (
        <Modal
          visible={showModal}
          onClose={() =>
            setShowModal(false)}
        >
          <UpdateForm />
        </Modal>
      )}
      <ChatsPanel />
      <MainWrapper>
        <UserInfoWrapper>
          <UserInfoItem onClick={() => {
            setShowModal(true)
          }}>
            {authorizedUser.displayName}
            {authorizedUser.photoURL && (<img src={authorizedUser.photoURL} width={36} height={36} />)}
            {authorizedUser.email}
            <Button onClick={logout}>Выйти</Button>
          </UserInfoItem>
          <SearchWrapper>
            <QueueWrapper>Клиентов в очереди: {pendingClients}</QueueWrapper>
            <Input
              name='search'
              type='search'
              placeholder={'Поиск'}
              value={searchText}
              onChange={(e) => handleSearchText(e.target.value)}
            />
          </SearchWrapper>
        </UserInfoWrapper>
        <DialogWrapper>
          <Switch>
            <Route exact path={PATHS.chat} component={Chat} />
            <Route exact path={PATHS.activeChats} component={ActiveDialogs} />
            <Route exact path={PATHS.completedChats} component={CompletedDialogs} />
            <Route exact path={PATHS.savedChats} component={SavedDialogs} />
            <Route><InfoWrapper> Select a chat to start messaging </InfoWrapper></Route>
          </Switch>
        </DialogWrapper>
      </MainWrapper>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100vh;  
  font-family: 'Varela-round', 'Nunito';
`

const MainWrapper = styled.div`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
`

const UserInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 95%;
  margin: 8px;
`
const UserInfoItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  box-sizing: content-box;
  cursor: pointer;
`

const QueueWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-right: 10px;
  font-size: 28px;
`

const SearchWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-left: 10px;
  margin-right: 10px;
`

const DialogWrapper = styled.div`
  display: flex;
  align-content: center;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  width: 95%;
  height: 78%;  
  background-color: #eeeeee;
  border: 1px solid #eeeeee;
  border-radius: 8px;
  margin: 20px;
  overflow-y:scroll;
`
const InfoWrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: auto;
    align-items: center;
`