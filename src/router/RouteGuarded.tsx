import React from 'react';
import { useSelector } from 'react-redux';
import { GlobalStore } from '../model/store';
import { Route, useHistory, RouteComponentProps } from "react-router-dom";
import { PATHS } from '../constants';

type Props = {
    component: React.ComponentType<RouteComponentProps<any>>,
    isPrivate?: boolean,
    path: string,
    exact?: boolean
}

export const RouteGuarded: React.FC<Props> = ({ component, isPrivate = false, path,exact = true }) => {
    const authorisedUser = useSelector((store: GlobalStore) => Boolean(store.authReducer.currentUser))
    const history = useHistory()
    React.useEffect(() => {
        if (isPrivate && !authorisedUser) {
            history.replace(PATHS.auth)
        }
        if (!isPrivate && authorisedUser) {
            history.replace(PATHS.main)
        }
    }, [history, authorisedUser])
    return (<Route exact={exact} component={component} path={path} />)
}