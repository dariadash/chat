import { BrowserRouter, Switch, Redirect } from 'react-router-dom';

import { PATHS } from '../constants';
import { Login } from '../pages/Login';
import { Register } from '../pages/Register';
import { Main } from '../pages/Main';

import { RouteGuarded } from './RouteGuarded';
import { UpdatePass } from "../pages/UpdatePass";
import { ForgotPass } from "../pages/ForgotPass";
import { useDispatch } from 'react-redux';
import { APP_LOAD } from '../sagas/actionTypes';

export const Router = () => {
    const dispatch = useDispatch()
    dispatch({ type: APP_LOAD })
    return (
        <BrowserRouter>
            <Switch>
                <RouteGuarded path={PATHS.auth} component={Login} />
                <RouteGuarded path={PATHS.register} component={Register} />
                <RouteGuarded path={PATHS.forgotpass} component={ForgotPass} />
                <RouteGuarded path={PATHS.updatepass} component={UpdatePass} />
                <RouteGuarded exact={false} path={PATHS.main} component={Main} isPrivate />
                <Redirect to={PATHS.auth} />
            </Switch>
        </BrowserRouter>
    );
};
