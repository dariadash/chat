export const APP_LOAD = 'APP_LOAD'

export const SIGNUP_REQUEST = "SIGNUP_REQUEST"

export const SIGNIN_SUCCESS = "SIGNIN_SUCCESS";
export const SIGNIN_ERROR = "SIGNIN_ERROR";
export const SIGNIN_REQUEST = "SIGNIN_REQUEST"
export const SIGNIN_GOOGLE_POPUP = "SIGNIN_GOOGLE_POPUP"

export const SIGNOUT_SUCCESS = "SIGNOUT_SUCCESS";
export const SIGNOUT_REQUEST = "SIGNOUT_REQUEST"

export const UPDATEPASS_REQUEST = 'UPDATEPASS_REQUEST'

export const RESETPASS_REQUEST = 'RESETPASS_REQUEST'

export enum Toasts {
    SHOW = "TOASTS_SHOW",
    CLEAR = "TOASTS_CLEAR"
}

export enum Chats {
    FETCH_ALL = "FETCH_ALL",
    SET_CHATS = "SET_CHATS",
    FILTER_CHATS = "FILTER_CHATS",
    SAVE_OR_UNSAVE_CHAT = "SAVE_OR_UNSAVE_CHAT",
    SET_SAVE_CHAT = "SET_SAVE_CHAT",
    TAKE_CHAT = "TAKE_CHAT",
}

export enum Messages {
    SEND_MESSAGE = 'SEND_MESSAGE',
    SEND_MESSAGE_LOCAL = 'SEND_MESSAGE_LOCAL',
    OPEN_CHAT = 'OPEN_CHAT',
    CLOSE_CHAT = 'CLOSE_CHAT',
}

export enum Phrase {
    SELECT_PHRASE = 'SELECT_PHRASE',
    SET_PHRASE = 'SET_PHRASE'
}

export enum Settings {
    UPDATE_SETTINGS = 'UPDATE_SETTINGS',
    UPDATE_SUCCESS = 'UPDATE_SUCCESS',
    UPDATE_FAIL = 'UPDATE_FAIL'
}