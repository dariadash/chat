import { call, put, take } from "redux-saga/effects";
import {
    getAuth,
    signInWithEmailAndPassword,
    UserCredential,
    createUserWithEmailAndPassword,
    signOut,
    User,
    confirmPasswordReset,
    sendPasswordResetEmail,
    GoogleAuthProvider,
    signInWithPopup
} from 'firebase/auth'
import {
    signinError,
    signinSuccess,
    signoutSuccess,
} from "../model/actions/auth";
import { showToast } from "../model/actions/toast";
import { ToastMessages } from "../features/toast/const";
import { EventChannel, eventChannel } from "@redux-saga/core";

// Providers
const auth = getAuth()
const googleProvider = new GoogleAuthProvider()
googleProvider.setCustomParameters({
    prompt: "select_account"
});

const getAuthChannel = () => eventChannel((emit) => {
    const unsubscribe = auth.onAuthStateChanged(user => emit({ user }));
    return unsubscribe;
});

export function* appLoadFx() {
    const channel: EventChannel<User> = yield call(getAuthChannel);
    const { user }: { user: User } = yield take(channel);
    if (user) {
        yield put(signinSuccess(user))
    }

}

export function* signInFx({ payload }: any) {
    try {
        const data: UserCredential = yield call(
            signInWithEmailAndPassword,
            auth,
            payload.email,
            payload.password
        )
        yield put(signinSuccess(data.user))
        yield put(showToast('You logged in!', 'success'))
    } catch (err) {
        const signinErr: { payload: string } = yield put(signinError());
        yield call(showToast, signinErr.payload, 'error');
    }
}

export function* signInGoogle() {
    try {
        const data: UserCredential = yield call(signInWithPopup, auth, googleProvider)
        yield put(signinSuccess(data.user))
        yield put(showToast('You logged in!', 'success'))
    }
    catch (err) {
        yield call(showToast, ToastMessages.signinError, 'error');
    }
}

export function* signUpFx({ payload }: any) {
    try {
        const data: UserCredential = yield call(
            createUserWithEmailAndPassword,
            auth,
            payload.email,
            payload.password
        )
        yield put(signinSuccess(data.user))
        yield put(showToast(ToastMessages.signupSuccess, 'success'))
    } catch (err) {
        yield call(showToast, ToastMessages.signupError, 'error');
    }
}

export function* signOutFx() {
    try {
        yield call(signOut, auth)
        yield put(showToast(ToastMessages.signoutSuccess, 'success'))
        yield put(signoutSuccess())
    } catch (err) {
        yield call(showToast, ToastMessages.signoutError, 'error');
    }
}

export function* resetPassFx({ payload }: any) {
    try {
        yield call(
            sendPasswordResetEmail,
            auth,
            payload.email, {
            url: 'http://localhost:3000/auth/updatepass'
        }
        )
        yield put(showToast(ToastMessages.resetEmailSuccess, 'success'))
    } catch (err) {
        yield call(showToast, ToastMessages.resetEmailError, 'error');
    }
}

export function* updatePassFx({ payload }: { type: string, payload: { oobKey: string, password: string } }) {
    const oobKey = payload.oobKey;
    const newPassword = payload.password;
    try {
        yield call(confirmPasswordReset, auth, oobKey, newPassword)
        yield call(showToast, ToastMessages.updatePassSuccess, 'success')
    } catch (err) {
        yield call(showToast, ToastMessages.updatePassError, 'error');
    }
}

