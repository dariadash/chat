import {
    collection,
    doc,
    getDocs,
    updateDoc,
    QuerySnapshot
} from "firebase/firestore";
import { call, put } from 'redux-saga/effects';
import { db } from "../services/firebase";
import { Chats } from "./actionTypes";

export function* fetchAllChatsFx({ payload }: any) {
    const data: QuerySnapshot = yield call(getDocs, collection(db, "users"))
    const rooms: any[] = []
    data.forEach((doc) => {
        rooms.push({
            ...doc.data(),
            roomDocumentId: doc.ref.id
        })

    })
    yield put({ type: Chats.SET_CHATS, payload: rooms })
}

export function* saveChatFx({ payload }: any) {
    const { roomId } = payload
    yield updateDoc(doc(db, 'users', roomId), {
        //change it
        "saved": true
    })
    yield put({ type: Chats.SET_SAVE_CHAT, payload })
}

export function* takeChatFx({ payload }: any) {
    const { roomId } = payload
    const upbateTaken = updateDoc(doc(db, 'users', roomId), {
        //change it
        "taken": true
    })
    yield (upbateTaken)
    yield put({ type: Chats.TAKE_CHAT, payload })
}



//To create or overwrite a single document, 
// use the set() method:
// Add a new document in collection "cities"
// await setDoc(doc(db, "cities", "LA"), {
//     name: "Los Angeles",
//     state: "CA",
//     country: "USA"
//   });

// If the document does exist, 
// its contents will be overwritten with the newly provided 
// data, unless you specify that the data should be 
// merged into the existing document, as follows:

// const cityRef = doc(db, 'cities', 'BJ');
// setDoc(cityRef, { capital: true }, { merge: true });



// To update some fields of a document without overwriting 
// the entire document, use the update() method:
// const washingtonRef = doc(db, "cities", "DC");

// // Set the "capital" field of the city 'DC'
// await updateDoc(washingtonRef, {
//   capital: true
// });



// deleting field
// const cityRef = doc(db, 'cities', 'BJ');

// // Remove the 'capital' field from the document
// await updateDoc(cityRef, {
//     capital: deleteField()
// });


//delete doc
//await deleteDoc(doc(db, "cities", "DC"));
