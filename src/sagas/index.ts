import { all, takeLatest } from "redux-saga/effects";
import {
    signInFx,
    signUpFx,
    signOutFx,
    signInGoogle,
    updatePassFx,
    resetPassFx,
    appLoadFx
} from './auth'
import {
    APP_LOAD,
    Chats,
    Messages,
    RESETPASS_REQUEST,
    SIGNIN_GOOGLE_POPUP,
    SIGNIN_REQUEST,
    SIGNOUT_REQUEST,
    SIGNUP_REQUEST,
    UPDATEPASS_REQUEST
} from "./actionTypes";
import { fetchAllChatsFx, saveChatFx, takeChatFx, } from "./chats";
import { createMessageFx } from "./message";


export default function* rootSaga() {
    yield all([
        takeLatest(SIGNUP_REQUEST, signUpFx),
        takeLatest(SIGNOUT_REQUEST, signOutFx),
        takeLatest(SIGNIN_REQUEST, signInFx),
        takeLatest(UPDATEPASS_REQUEST, updatePassFx),
        takeLatest(RESETPASS_REQUEST, resetPassFx),
        takeLatest(SIGNIN_GOOGLE_POPUP, signInGoogle),
        takeLatest(APP_LOAD, appLoadFx),
        takeLatest(Chats.FETCH_ALL, fetchAllChatsFx),
        takeLatest(Chats.SAVE_OR_UNSAVE_CHAT, saveChatFx),
        takeLatest(Chats.TAKE_CHAT, takeChatFx),
        takeLatest(Messages.SEND_MESSAGE, createMessageFx),
    ])
}
