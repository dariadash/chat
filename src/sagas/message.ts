import { arrayUnion, doc, updateDoc } from "@firebase/firestore";
import { put, select } from "@redux-saga/core/effects";
import { GlobalStore } from "../model/store";
import { db } from "../services/firebase";
import { Messages } from "./actionTypes";

/*
   @param {string} payload - Сообщение, которое требуется отправить
*/
export function* createMessageFx({ payload }: any) {
    const store: GlobalStore = yield select()
    if (!store.messagesReducer.currentRoom) {
        throw new Error('current room doesnt exist')
    }
    const { roomDocumentId } = store.messagesReducer.currentRoom
    yield put({
        type: Messages.SEND_MESSAGE_LOCAL, payload: {
            // picture: payload,
            content: payload,
            writtenBy: store.authReducer.currentUser?.email ?? store.authReducer.currentUser?.displayName
        }
    })
    yield updateDoc(doc(db, 'users', roomDocumentId), {
        messages: arrayUnion({
            // picture: payload,
            content: payload,
            writtenBy: store.authReducer.currentUser?.email,
            timestamp: new Date()
        })
    })
}
