import { getAuth, updateProfile, UserCredential, User } from "firebase/auth";
import { call, put } from 'redux-saga/effects';
import { updateError, updateSuccess } from "../model/actions/settings";
import { showToast } from "../model/actions/toast";
import { ToastMessages } from "../features/toast/const";

const auth = getAuth();
export function* updateprofileFx({ payload }: any) {
    try {
        const data: UserCredential = yield call(
            updateProfile,
            auth.currentUser as User,
            {
                displayName: "Jane Q. User",
                photoURL: "https://example.com/jane-q-user/profile.jpg"
            })
        yield put(updateSuccess(data.user))
        yield put(showToast(ToastMessages.updateSettingsSuccess, 'success'))
    } catch (err) {
        const updateErr: { payload: string } = yield put(updateError());
        yield call(showToast, updateErr.payload, 'error');
    }
}
