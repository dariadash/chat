import { getFirestore } from "firebase/firestore";
import {initializeApp} from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyDy65nZ3zRhlhbCV3TDG5Ro28mfduGA12U",
    authDomain: "chat-2d215.firebaseapp.com",
    projectId: "chat-2d215",
    storageBucket: "chat-2d215.appspot.com",
    messagingSenderId: "790380273181",
    appId: "1:790380273181:web:7b23c609c084601302b0f4",
    measurementId: "G-DJ8H6GHEM8"
};

initializeApp(firebaseConfig);


export const db = getFirestore();