import React from "react"
import styled, { css } from "styled-components"

type Props = {
    type?: 'submit' | 'reset' | 'button' | undefined;
    onClick?: (e?: any) => void,
    isCircle?: boolean,
    size?: string,
    background?: string
    primary?: boolean,
}
export const Button: React.FC<Props> = ({
    children,
    onClick,
    isCircle,
    primary,
    background,
    type
}) => {
    return (
        <ButtonWrapper
            type={type}
            primary={primary}
            onClick={onClick}
            isCircle={isCircle}
            background={background}
        >
            {children}
        </ButtonWrapper>
    )
}

type ButtonProps = {
    isCircle?: boolean,
    size?: string,
    background?: string
    primary?: boolean
}

const ButtonWrapper = styled.button<ButtonProps>`
    display: flex;
    font-family: 'Nunito';  
    border: none;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    color: #FFF;
    ${({ size }) => !size && css`
        font-size: 20px;
    `}
    font-size: 16px;
    margin: 10px;
    padding: 10px;
    padding-left: 30px;
    padding-right: 30px;
    border-radius: 64px;
    ${({ background }) => !background && css`
        background: #007bff;
    `}
    cursor: pointer;
    &:hover {
        background: #0062cc;
        transition: 0.3s;
    }
    ${({ isCircle }) => isCircle && css`
        width: 24px;
        height: 24px;
        padding: 10px;
    `}
    ${({ primary }) => primary && css`
        background: #2e89ff;
        &:hover {
            background: #215fb1;
        }
    `}
`