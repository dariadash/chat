import React from "react";
import styled from "styled-components";
import { Icon } from "./Icon";

type Props = {
    options: {
        value: number | string,
        text: string,
    }[],
    placeholder?: string,
    onOptionChange: (optionValue: number | string) => void
}

export const Dropdown = ({ options, onOptionChange, placeholder = 'Не выбрано' }: Props) => {
    const [isOpen, setIsOpen] = React.useState(false);
    const toggleList = () => setIsOpen(!isOpen)

    const onOptionClicked = (item: any) => {
        onOptionChange(item.value)
        toggleList()
    };

    return (
        <DropDownContainer>
            <DropDownHeader onClick={toggleList}>
                <div>{placeholder}</div>
                <Icon icon='down' />
            </DropDownHeader>
            {isOpen && (
                <DropDownListContainer>
                    <DropDownList>
                        {options.map((item) => (
                            <ListItem
                                key={item.value}
                                onClick={() => onOptionClicked(item)}
                            >
                                {item.text}
                            </ListItem>
                        ))}
                    </DropDownList>
                </DropDownListContainer>
            )}
        </DropDownContainer>
    )
}

const DropDownContainer = styled.div`
    width: 240px;
`;

const DropDownHeader = styled.div`
    border-radius: 8px;
    border: 3px solid #EEE;
    padding: 14px;
    margin: 4px;
    border-radius: 8px;
    font-weight: 500;
    color: #111;   
    background: #fff;
    cursor: pointer;
    user-select: none;
    display: flex;
    justify-content: space-between;
    align-items: center;
    &:hover {
        border: 3px solid #DDD;
        transition: 0.3s;
    }
`;

const DropDownListContainer = styled.div`
    position: absolute;
    border-radius: 8px;
    margin-top: 4px;
    width: 240px;
`;

const DropDownList = styled.ul`
    top: 100%;
    padding: 0;
    margin: 0;
    background: #ffffff;
    
    border: 5px solid #FAFAFA;
    border-radius: 8px;

    box-shadow: 0px 12px 24px 2px #11111133;
    box-sizing: border-box;
    color: #007bff;
    font-size: 14px;
  animation: fadeout 0.5s;
`;

const ListItem = styled.li`
  list-style: none;
  padding-left: 8px;
  padding-right: 8px;
  padding-bottom: 12px;
  padding-top: 12px;
  color: #111;
  animation: fadeout 0.5s;
  border-radius: 4px;
  user-select: none;
  &:hover {
      background-color: #eff4ff;
      cursor: pointer; 
      transition: 0.5s;
  }
`;