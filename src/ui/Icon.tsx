import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faTrash,
    faCheck,
    faTimesCircle,
    faSave,
    faEdit,
    faStar,
    faExclamation,
    faCog,
    faSortDown,
    faSmileBeam,
    faPlusSquare
} from '@fortawesome/free-solid-svg-icons'
import { SizeProp } from "@fortawesome/fontawesome-svg-core";

const Icons = {
    'edit': faEdit,
    'save': faSave,
    'delete': faTrash,
    'done': faCheck,
    'close': faTimesCircle,
    'favs': faStar,
    'important': faExclamation,
    'settings': faCog,
    'down': faSortDown,
    'emoji': faSmileBeam,
    'add': faPlusSquare
}

export type IconType = keyof typeof Icons

type Props = {
    icon: IconType
    size?: SizeProp
    onClick?: () => void
    style?: Object
    color?: string
}

export const Icon = ({ icon, size, onClick, color }: Props) => {
    return <FontAwesomeIcon icon={Icons[icon]} size={size} onClick={onClick} color={color} />
}