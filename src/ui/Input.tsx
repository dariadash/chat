import React from "react";
import styled, { css } from "styled-components"

type Props = {
    name: string,
    type: string,
    placeholder: string,
    value: string,
    onChange: (e?: any) => void,
    width?: number
}

export const Input: React.FC<Props> = ({ name, type, placeholder, value, onChange, width }) => {
    return (
        <InputWrapper
            name={name}
            type={type}
            placeholder={placeholder}
            width={width}
            value={value}
            onChange={onChange}
        />
    )
}

type WrapperProps = {
    width?: number
}

export const InputWrapper = styled.input<WrapperProps>`
${({ width }) => width && css`
    width: ${width}%;
`}
  flex-direction: row;
  font-size: 16px;
  word-break: break-word;
  padding: 10px;
  margin-left: 10px;
  border-radius: 8px;
  background: #FFFFFF;
  border: 1px solid #CCC;
  color: #080111;
  &:focus {
        outline: none;
        box-shadow: 0px 0px 2px #007bff;
    }
`