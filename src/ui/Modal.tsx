import React from 'react';
import styled, { css } from 'styled-components';

type Props = {
    visible: boolean,
    onClose: () => void,
    top?: number
}

export const Modal: React.FC<Props> = ({ visible, onClose, children, top }) => {
    if (!visible) {
        return null;
    }
    return (
        <Overlay top={top} onClick={() => onClose()}>
            <Container top={top} onClick={(e) => e.stopPropagation()}>
                <div>{children}</div>
            </Container>
        </Overlay>
    );
}

type ContainerProps = {
    top?: number
}

const Container = styled.div<ContainerProps>`
    border: 3px solid #999;
    background-color: #fff;
    width: 85vw;
    min-width: 320px;
    max-width: 600px;
    overflow-y: auto;
    max-height: calc(100vh - 210px);
    box-shadow: 0, 0, 8px, #111;
    border-radius: 8px;
    ${({ top }) => top && css`
        margin-top: ${top}px;
    `}
`

const Overlay = styled.div<ContainerProps>`
    z-index: 20;
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    position:fixed; 
    top: 0;
    left: 0;
    background-color: #11111199;
    overflow: hidden;
    ${({ top }) => top && css`
        align-items: flex-start;
    `}
`