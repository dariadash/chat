import React from "react";
import styled from "styled-components";
import { Icon } from "./Icon";

type Props = {
    // options: {
    //     value: number | string,
    //     text: string,
    // }[],
    // onOptionChange: (optionValue: number | string) => void
}

export const Popper: React.FC<Props> = ({ children }) => {
    const [isOpen, setIsOpen] = React.useState(false);
    const toggleList = () => setIsOpen(!isOpen)

    return (
        <PopperContainer onClick={toggleList}>
            <Icon icon='emoji' />
            {isOpen &&
                <> {children}</>
            }
        </PopperContainer>
    )
}

const PopperContainer = styled.div`
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    position: absolute ; 
    overflow: hidden;
`;

