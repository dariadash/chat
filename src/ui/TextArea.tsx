import React from "react"
import styled, { css } from "styled-components"

type Props = {
    placeholder?: string
    value?: string,
    onChange: (text: string) => void
    width?: number
}

export const TextArea = ({ placeholder, onChange, value, width }: Props) => {
    return (
        <InputWrapper
            placeholder={placeholder}
            value={value}
            onChange={(e) => onChange(e.target.value)}
            rows={5}
            width={width}
        />
    )
}

type WrapperProps = {
    width?: number
}

const InputWrapper = styled.textarea<WrapperProps>`
${({ width }) => width && css`
    width: ${width}%;
`}
  flex-direction: row;
  font-size: 14px;
  padding: 10px;
  border-radius: 8px;
  background: #FFFFFF;
  border: 1px solid #CCC;
  color: #080111;
  &:focus {
        outline: none;
        box-shadow: 0px 0px 2px #007bff;
    }
`