
import React from "react";
import {render} from 'enzyme'
import {Input} from "./index";

const testInputData = "MY_TEST_INPUT_DATA"

describe('Input value test', () => {
    const inputData = {
        name: "TestInput",
        type: "text",
        placeholder:"sample",
        onChange: () => ({}),
        value: testInputData
    }
    const inputElement = render(<Input {...inputData }/>)
    it('render initial', () => {
        expect(inputElement.val()).toEqual(testInputData)
    })
})